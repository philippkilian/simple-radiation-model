#!/bin/python3
# -*- coding: utf-8 -*-
import random as rd
import math
# --------------------------------------
# Simple Radiation Model
# --------------------------------------

tau = 10.0
dtau = 0.05
rho = 0.05
theta = math.pi/9.0
n = 1000
gmin = -1.0
gmax = 1.0
dg = 0.02
wmin = 0.0
wmax = 1.0
dw = 0.02
nLmax = nL = tau/dtau

outfile = open('radiation_data_output', 'a')
outfile.write('g  ' + 'w  ' + 'nTOA  ' + 'nBOA  ' + 'nABS  ' + 'nABB  ' + '\n')
g = gmin
runtime = 0
while g <= gmax:
    w = wmin
    
    while w <= wmax:
        nTOA = 0; nBOA = 0; nABS = 0; nABB = 0
        
        for i in rage(n):
            alpha = theta
            mu = math.cos(theta)
            a = 0
            j = 0
            
            if mu <= 0:
                mu = -1*mu
                
            while (a == 0) == True:
                
                if nL == 0:
                    r = rd.random()
                    
                    if j == 0:
                        nBOA += 1
                        j = 1
                        
                        if r > rho:
                            a = 1
                            nABB += 1
                            break
                        else:
                            nL = 1
                            alpha = 2*math.pi-alpha
                            break
                    
                    if j != 0:
                        
                        if r > rho:                
                            a = 1
                            nABB += 1
                            break
                        else:
                            nL = 1
                            alpha = 2*math.pi-alpha
                            break
                
                if nL > nLmax:
                    nTOA += 1
                    a = 1
                    break
                    
                r = rd.random()
                if r <= math.exp(-1*(dtau/mu)):
                    
                    if alpha < 1.5*math.pi and alpha > 0.5*math.pi:
                        nL += 1
                    else:
                        nL -= 1
                    break
                
                if r > math.exp(-1*(dtau/mu)):
                    r = rd.random()
                    
                    if r > w:
                        nABS += 1
                        a=1
                        break
                    else:
                        r = rd.random()
                        
                        if g >= -0.01 and g <= 0.01:
                            beta = math.pi*r
                        else:
                            beta = math.acos((1+g**2-((1-g**2)/(1-g+2*g*r))**2)/(2*g))
                        
                        r = rd.random()
                        
                        if r <= 0.5:
                            beta = -1*beta
                        
                        alpha = alpha + beta
                        
                        if alpha < 0:
                            alpha = alpha+2*math.pi
                            
                        if alpha > 2*math.pi:
                            alpha = alpha-2*math.pi
                            
                        if alpha == 0.5*math.pi or alpha == 1.5*math.pi:
                            break
                        
                        if alpha < 1.5*math.pi and alpha > 0.5*math.pi:
                            nL += 1
                        else:
                            nL -= 1
                        break
            
            mu = math.cos(alpha)

        outfile.write(str(g) + '  ' + str(w) + '  ' + str(nTOA) + '  ' + str(nBOA) + ' ' + str(nABS) + '  ' + str(nABB) + '\n')
        w += dw
        
    print(runtime)
    g += dg
    runtime += 1

outfile.close()
